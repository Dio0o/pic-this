package org.academiadecodigo.bootcamp67.utilities;

import java.util.concurrent.TimeUnit;

public class TimeCheck {
    public static long getTime(){
        return TimeUnit.MILLISECONDS.toSeconds(System.currentTimeMillis());
    }
}
