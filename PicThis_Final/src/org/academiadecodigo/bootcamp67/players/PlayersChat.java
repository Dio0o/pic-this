package org.academiadecodigo.bootcamp67.players;

import org.academiadecodigo.bootcamp67.prints.GameInfo;
import org.academiadecodigo.bootcamp67.servers.GameServer;

import java.io.*;
import java.net.InetAddress;
import java.net.Socket;
import java.util.List;

import static org.academiadecodigo.bootcamp67.Game.gameRunning;

public class PlayersChat implements Runnable{
    private final GameServer gameServer;
    private final Socket socket;

    private BufferedReader in;
    private PrintStream out;
    private String nickName;
    private boolean ready;
    private boolean alreadyGuessed;
    private long score;
    /*private boolean connectedToImageServer;*/


    public PlayersChat(Socket socket, GameServer gameServer) {
        /*this.connectedToImageServer = false;*/
        this.socket = socket;
        this.gameServer = gameServer;
        this.score = 0;
        this.ready = false;
        try {
            this.out = new PrintStream(socket.getOutputStream(), true);
            this.in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public InetAddress getPlayerIp(){
        return socket.getInetAddress();
    }

    /*public void setPlayerConnectedToImageServer(){
        connectedToImageServer = true;
    }*/


    private void askIfReady() {
        out.println(GameInfo.CHECK_READY);
        while(!ready) {
            try {
                if (in.readLine().contains("yes")) {
                    this.ready = true;
                    gameServer.aPlayerIsReady();
                }
            } catch (IOException e) {
                System.exit(1);
            }
        }
    }

    public synchronized void checkNewConnectionToImageServer(){
        this.notify();
    }



    public boolean isReady() {
        return ready;
    }


    public void sendText(String text){
        out.println(text);
    }

    private boolean isPlayerConnectedToImageServer(){
        System.out.println(gameServer.getListOfPlayersConnectedToImageServer() + " " + socket.getInetAddress());
        return gameServer.getListOfPlayersConnectedToImageServer().contains(socket.getInetAddress());
    }



    public String getNickName() {
        return nickName;
    }
    //While player is connected to game player is able to type to everyone connected as if it was a chat

    @Override
    public void run() {
        greeting();
        out.println("\n" + GameInfo.PICK_NICK + "\n");
        setNickName();
        out.println("\n" + GameInfo.RULES + "\n");
        try {
            Thread.sleep(1500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        out.println(GameInfo.CONNECT_TO_IMAGE_SERVER);
        synchronized (this) {
            while (!isPlayerConnectedToImageServer()) {
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }


        askIfReady();

        out.println(GameInfo.WAIT);

        try {
            while(gameRunning){
                gameServer.sendAll(this, in.readLine());
            }
            gameServer.sendHighestScoreAll(this);
            out.println(GameInfo.sendTotalScore(nickName, score));
        } catch (IOException  e) {
            System.out.println("Player has disconnected.");
        }
    }



    private void setNickName()  {
        try {
            this.nickName = in.readLine();
        } catch (IOException e) {
            System.out.println("Unable to get userName from player");
        }

        if (!nickName.matches("^[\\w.-_]{3,10}$")) {
            out.println(GameInfo.INVALID_NICK);
            setNickName();
            return;
        }
        if(gameServer.getNickNames().contains(nickName)){
            out.println("That name is already in use!");
            setNickName();
            return;
        }
        gameServer.addName(nickName);
                                // Test to see who entered the game
    }


    public void giveScore(long scoreForGuessing) {
        score += scoreForGuessing;
    }


    public void setGuessed(boolean guessed) {
        alreadyGuessed = guessed;
    }


    public boolean isAlreadyGuessed() {
        return alreadyGuessed;
    }


    public Long getTotalScore() {
        return score;
    }


    private void greeting(){
        String[] gameName = GameInfo.GAME_NAME.split("");
        printDrawing(gameName);
    }


    private void printDrawing(String[] art){
        for (String characters : art) {
            out.print(characters);
            try {
                Thread.sleep(GameInfo.GAME_NAME_DELAY);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }


    public PrintStream getOut() {
        return out;
    }


    public void printLoadingBar() throws IOException {
        FilterOutputStream outPut = new FilterOutputStream(socket.getOutputStream());
        for (int i = 0; i < 30; i++){
            outPut.write(178);
            try {
                Thread.sleep(30);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
