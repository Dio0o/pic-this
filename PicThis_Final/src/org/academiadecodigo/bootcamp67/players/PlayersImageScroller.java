package org.academiadecodigo.bootcamp67.players;

import org.academiadecodigo.bootcamp67.prints.GameInfo;
import org.academiadecodigo.bootcamp67.prints.Images;


import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Arrays;

import static org.academiadecodigo.bootcamp67.Game.allowedToDraw;


public class PlayersImageScroller implements Runnable{
    private PrintStream out;
    private static String currentDrawing;
    private static String currentWord;
    public static boolean canRequestNewImage = true;


    public PlayersImageScroller(Socket playerConnection) {
        try {
            this.out = new PrintStream(playerConnection.getOutputStream(), true);
        } catch (IOException e) {
            System.out.println("Unable to send images to player");
        }
    }



    public synchronized static void setCurrentImage(Images currentImage) {
        currentWord = currentImage.getWord();
        currentDrawing = currentImage.getDrawing();
        canRequestNewImage = false;
    }

    private synchronized void print() throws InterruptedException {
        int index = 0;
        String[] art = currentDrawing.split("");
        while (allowedToDraw) {
            if (index < art.length) {
                out.print(art[index]);
                index++;
                wait(15);
            } else {
                return;
            }
        }
        out.println(currentDrawing.substring(index));
    }



    @Override
    public void run() {
        synchronized (this) {
            try {
                print();
                out.println(GameInfo.writeWord(currentWord));
                out.println(GameInfo.SPACING);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
