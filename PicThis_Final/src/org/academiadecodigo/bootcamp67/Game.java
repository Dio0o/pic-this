package org.academiadecodigo.bootcamp67;

import org.academiadecodigo.bootcamp67.players.PlayersChat;
import org.academiadecodigo.bootcamp67.prints.GameInfo;
import org.academiadecodigo.bootcamp67.servers.GameServer;
import org.academiadecodigo.bootcamp67.servers.ImageServer;
import org.academiadecodigo.bootcamp67.utilities.TimeCheck;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class Game {


    private final int NUM_IMAGES_PER_GAME;
    private final ExecutorService cachedPool;
    private final GameServer gameServer;
    private final ImageServer imageServer;
    private boolean allPlayersHaveGuessed;
    private boolean allPlayersReady;
    public volatile static boolean allowedToDraw;
    public volatile static boolean gameRunning;



    public Game(int NUM_IMAGES_PER_GAME) {
        this.NUM_IMAGES_PER_GAME = NUM_IMAGES_PER_GAME;
        this.cachedPool = Executors.newCachedThreadPool();
        this.gameServer = new GameServer(this);
        this.imageServer = new ImageServer(gameServer);

    }


    private void init(){
        cachedPool.submit(gameServer);
        cachedPool.submit(imageServer);
        gameRunning = true;
        this.allPlayersHaveGuessed = false;
        this.allPlayersReady = false;
    }


    public void start() {
        init();
        synchronized (this){
            while (!allPlayersReady){
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
        playGame();
    }

    public synchronized void readyCheck(){
        this.notify();
    }

    public void setAllPlayersReady(){
        allPlayersReady = true;
    }



    private synchronized void playGame() {

        cachedPool.shutdownNow();

        int numOfImagesPlayed = 0;
        while (numOfImagesPlayed < NUM_IMAGES_PER_GAME) {
            try {
                wait(100);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            conditionsBeforePrinting();
            imageServer.startPrintingDrawing();
            numOfImagesPlayed++;
            gameServer.setTimeWhenImageStartedPrinting(TimeCheck.getTime());
            waitForPlayersToGuessWord();
        }

        allowPlayersToUseChat();

        gameServer.systemMessage(GameInfo.FINISH_GAME);
        gameServer.systemMessage(GameInfo.FINAL_SCORES);
        gameRunning = false;

    }

    private void conditionsBeforePrinting(){
        allowedToDraw = true;
        allPlayersHaveGuessed = false;
        gameServer.setCurrentWord(imageServer.getNewImage().getWord());
        gameServer.systemMessage(GameInfo.START_GAME);
        allowPlayersToUseChat();
    }

    private void waitForPlayersToGuessWord(){
        synchronized (this){
            while (!allPlayersHaveGuessed){
                try {
                    wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    private void allowPlayersToUseChat(){
        for (PlayersChat player : gameServer.playersConnected()) {
            player.setGuessed(false);
        }
    }


    public void setAllPlayersHaveGuessed() {
        this.allPlayersHaveGuessed = true;
        allowedToDraw = false;
    }
}
