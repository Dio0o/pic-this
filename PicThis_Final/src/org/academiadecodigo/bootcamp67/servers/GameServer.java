package org.academiadecodigo.bootcamp67.servers;

import org.academiadecodigo.bootcamp67.Game;
import org.academiadecodigo.bootcamp67.players.PlayersChat;
import org.academiadecodigo.bootcamp67.prints.GameInfo;
import org.academiadecodigo.bootcamp67.utilities.TimeCheck;

import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static java.lang.System.out;

public class GameServer implements Runnable{
    private final List<InetAddress> playersConnectedToImageServer;
    private final List<PlayersChat> playersChats;
    private final List<String> nickNames;
    private final Game game;
    private String currentWord;
    private ServerSocket gameServer;
    private final int MAX_POINTS_PER_ANSWER;
    private long timeWhenImageStartedPrinting;
    private int numOfPlayersThatGuessedRight;


    public GameServer(Game game) {
        this.game = game;
        this.playersConnectedToImageServer = new LinkedList<>();
        this.MAX_POINTS_PER_ANSWER = 30;
        this.nickNames = new LinkedList<>();
        this.playersChats = new LinkedList<>();
        this.numOfPlayersThatGuessedRight = 0;
        try {
            this.gameServer = new ServerSocket(5000);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<InetAddress> getListOfPlayersConnectedToImageServer(){
        return playersConnectedToImageServer;
    }


    public void addIpOfNewPlayerConnectedToImageServer(InetAddress newPlayer){
        playersConnectedToImageServer.add(newPlayer);
    }

    public void systemMessage(String message){
        for (PlayersChat player : playersChats) {
            player.sendText(message);
        }
    }

    public void newPlayerConnectedToImageServer(){
        for (PlayersChat player : playersChats) {
            player.checkNewConnectionToImageServer();
        }
    }



    public void sendAll(PlayersChat playerSending, String text) {
        if (playerSending.isAlreadyGuessed()){
            playerSending.sendText(GameInfo.CHAT_DENIED);
            return;
        }
        if (text.toLowerCase().contains(currentWord) && !playerSending.isAlreadyGuessed()){
            long pointsWon = MAX_POINTS_PER_ANSWER - (TimeCheck.getTime() - timeWhenImageStartedPrinting);
            playerSending.giveScore(pointsWon);
            playerSending.sendText(GameInfo.CORRECT);
            playerSending.sendText(GameInfo.sendScore(pointsWon));
            playerSending.setGuessed(true);
            numOfPlayersThatGuessedRight++;
            if(numOfPlayersThatGuessedRight == playersChats.size()){
                synchronized (game){
                    numOfPlayersThatGuessedRight = 0;
                    game.setAllPlayersHaveGuessed();
                    game.notifyAll();}

            }
        }
        for (PlayersChat player : playersChats) {
            if (playerSending != player && player.isReady() && !text.toLowerCase().contains(currentWord) && !text.equals("")){              //Sends typed text to all players except to itself
                player.sendText(playerSending.getNickName() + ": " +text);
            }
        }
    }


    public void sendHighestScoreAll(PlayersChat player) {
        long highestScore = 0;
        String playerNick = "";
        for (PlayersChat playerNum : playersChats) {
            if (playerNum.getTotalScore() > highestScore) {
                highestScore = playerNum.getTotalScore();
                playerNick = playerNum.getNickName();
            }
        }
        player.getOut().println(GameInfo.sendWinner(playerNick));

    }

    public List<String> getNickNames(){
        return nickNames;
    }

    public List<PlayersChat> playersConnected() {
        return playersChats;
    }

    public void addName(String nickName){
        this.nickNames.add(nickName);
    }

    private void waitingForPlayersToConnect() {
        ExecutorService cached = Executors.newCachedThreadPool();
        try {
            while(Thread.currentThread().isAlive()) {
                Socket socket = gameServer.accept();
                PlayersChat playerConnection = new PlayersChat(socket, this);
                playersChats.add(playerConnection);
                cached.submit(playerConnection);
            }
            out.println("Stopped listening for players");
        } catch (IOException e) {
            out.println("Unable to get player");
        }
    }

    @Override
    public void run() {
        waitingForPlayersToConnect();
    }

    public void setCurrentWord(String word) {
        this.currentWord = word;
    }

    public void setTimeWhenImageStartedPrinting(long timeWhenImageStartedPrinting) {
        this.timeWhenImageStartedPrinting = timeWhenImageStartedPrinting;
    }

    public void aPlayerIsReady() {
        for (PlayersChat player : playersChats) {
            if (!player.isReady()){
                return;
            }
        }
        game.setAllPlayersReady();
        game.readyCheck();
    }

}
