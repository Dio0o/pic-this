package org.academiadecodigo.bootcamp67.servers;

import org.academiadecodigo.bootcamp67.players.PlayersImageScroller;
import org.academiadecodigo.bootcamp67.prints.Images;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ImageServer implements Runnable{
    private final List<PlayersImageScroller> playersConnectedToImageServer;
    private final ExecutorService cached;
    private LinkedList<Images> listOfImages;
    private ServerSocket imageServer;
    private final GameServer gameServer;
    private int indexOfImage = 0;


    public ImageServer(GameServer gameServer) {
        this.playersConnectedToImageServer = new LinkedList<>();
        this.cached = Executors.newCachedThreadPool();
        this.gameServer = gameServer;
        try {
            this.imageServer = new ServerSocket(5050);
        } catch (IOException e) {
            System.out.println("Unable to create ImageServer socket");
        }
    }

    public Images getNewImage(){
        Images randomImage = listOfImages.get(indexOfImage);
        PlayersImageScroller.setCurrentImage(randomImage);
        indexOfImage++;
        return randomImage;
    }


    public void startPrintingDrawing(){
        for (PlayersImageScroller connectionToImageServer : playersConnectedToImageServer) {
            cached.execute(connectionToImageServer);
        }
    }


    public List<PlayersImageScroller> getPlayersConnectedToImageServer() {
        return playersConnectedToImageServer;
    }


    @Override   //Will wait for all players to connect to image server
    public void run() {
        listOfImages = new LinkedList<>(EnumSet.allOf(Images.class));              //adds all elements from and enum to the list **TESTED**
        Collections.shuffle(listOfImages);

        while (!imageServer.isClosed()){
            try {
                Socket socket = imageServer.accept();
                PlayersImageScroller newConnection = new PlayersImageScroller(socket);
                playersConnectedToImageServer.add(newConnection);
                cached.submit(newConnection);
                gameServer.addIpOfNewPlayerConnectedToImageServer(socket.getInetAddress());
                gameServer.newPlayerConnectedToImageServer();

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }


}
