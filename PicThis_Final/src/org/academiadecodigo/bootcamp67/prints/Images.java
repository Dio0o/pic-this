package org.academiadecodigo.bootcamp67.prints;

import org.academiadecodigo.bootcamp67.prints.asciiArt.ASCIIAnimalArt;

public enum Images {
    BAT("bat", ASCIIAnimalArt.BAT),
    BISON("bison", ASCIIAnimalArt.BISON),
    EAGLE("eagle", ASCIIAnimalArt.EAGLE),
    PEACOCK("peacock", ASCIIAnimalArt.PEACOCK),
    AARDVARK("aardvark", ASCIIAnimalArt.AARDVARK),
    ELEPHANT("elephant", ASCIIAnimalArt.ELEPHANT),
    DOLPHIN("dolphin", ASCIIAnimalArt.DOLPHIN),
    MONKEY("monkey", ASCIIAnimalArt.MONKEY),
    CAT("cat", ASCIIAnimalArt.CAT),
    DOG("dog", ASCIIAnimalArt.DOG);


    final String word;
    final String drawing;

    Images(String word, String drawing){
        this.word = word;
        this.drawing = drawing;
    }

    public String getWord() {
        return word;
    }

    public String getDrawing() {
        return drawing;
    }
}
