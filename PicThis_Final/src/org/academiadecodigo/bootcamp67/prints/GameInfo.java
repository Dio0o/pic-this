package org.academiadecodigo.bootcamp67.prints;

public class GameInfo {

    public static final int GAME_NAME_DELAY = 15;
    static final String RESET = "\033[0m";  // Text Reset



    public static final String YELLOW = "\033[0;33m";  // YELLOW
    public static final String GREEN_BRIGHT = "\033[0;92m";  // GREEN
    public static final String YELLOW_BRIGHT = "\033[0;93m"; // YELLOW
    public static final String PURPLE_BRIGHT = "\033[0;95m"; // PURPLE
    public static final String BLACK_BOLD_BRIGHT = "\033[1;90m"; // BLACK
    public static final String RED_BOLD_BRIGHT = "\033[1;91m";   // RED
    public static final String GREEN_BOLD_BRIGHT = "\033[1;92m"; // GREEN
    public static final String YELLOW_BOLD_BRIGHT = "\033[1;93m";// YELLOW
    public static final String BLUE_BOLD_BRIGHT = "\033[1;94m";  // BLUE
    public static  final String PURPLE_BOLD_BRIGHT = "\033[1;95m";// PURPLE
    public static final String CYAN_BOLD_BRIGHT = "\033[1;96m";  // CYAN
    public static final String WHITE_BOLD_BRIGHT = "\033[1;97m"; // WHITE




    public static final String SPACING = "\n\n\n\n\n";

    public static final String ASTERISKS = WHITE_BOLD_BRIGHT + "*" + BLACK_BOLD_BRIGHT + "*" + WHITE_BOLD_BRIGHT + "*" + BLACK_BOLD_BRIGHT + "*" + WHITE_BOLD_BRIGHT + "*";


    public static final String INVALID_NICK = "\n               " + ASTERISKS + RED_BOLD_BRIGHT + " INVALID NICK " + ASTERISKS + "\n" +
            BLACK_BOLD_BRIGHT + "       (Nick range between " + YELLOW + "3 " + BLACK_BOLD_BRIGHT + "and " + YELLOW + "10 " + BLACK_BOLD_BRIGHT + "characters)" + "\n" +
            "       (Special characters accepted: (" + YELLOW + "." + BLACK_BOLD_BRIGHT + ")(" + YELLOW + "_" + BLACK_BOLD_BRIGHT + ")(" + YELLOW + "-" + BLACK_BOLD_BRIGHT + "))" + RESET + "\n";


    public static final String PICK_NICK = "\n" + "              " + ASTERISKS + YELLOW_BOLD_BRIGHT + " PICK YOUR NICK " + ASTERISKS + "\n" +
            "     " + BLACK_BOLD_BRIGHT + "(This means that you have to write a name...)" + RESET + "\n";


    public static final String GAME_NAME =  RED_BOLD_BRIGHT + " _____ _      _                    _______ _     _     \n" +
            YELLOW_BOLD_BRIGHT + "|  __ (_)    | |                  |__   __| |   (_)    \n" +
            GREEN_BOLD_BRIGHT + "| |__) |  ___| |_ _   _ _ __ ___     | |  | |__  _ ___ \n" +
            CYAN_BOLD_BRIGHT + "|  ___/ |/ __| __| | | | '__/ _ \\    | |  | '_ \\| / __|\n" +
            BLUE_BOLD_BRIGHT + "| |   | | (__| |_| |_| | | |  __/    | |  | | | | \\__ \\\n" +
            PURPLE_BOLD_BRIGHT + "|_|   |_|\\___|\\__|\\__,_|_|  \\___|    |_|  |_| |_|_|___/" + RESET;


    public static final String RULES = YELLOW_BOLD_BRIGHT + "   _______________________________________________\n" +
            " / \\                                              \\" + RED_BOLD_BRIGHT + ".\n" +
            YELLOW_BOLD_BRIGHT + "|   |" + RESET + "                    " + WHITE_BOLD_BRIGHT + "RULES" + RESET + "                    " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            " " + YELLOW_BOLD_BRIGHT + "\\_ |" + RESET + " ******************************************* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " **                                       ** " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " *  " + YELLOW_BRIGHT + " A SET OF PICTURES WILL BE DISPLAYED   " + RESET + "* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " *  " + YELLOW_BRIGHT + "          AND YOU'LL HAVE TO           " + RESET + "* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " *  " + YELLOW_BRIGHT + "       GUESS BEFORE THE OTHERS!        " + RESET + "* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " *  " + GREEN_BRIGHT + "                                       " + RESET + "* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " *  " + RED_BOLD_BRIGHT + "        SNITCHES GET STITCHES!         " + RESET + "* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " *  " + GREEN_BRIGHT + "                                       " + RESET + "* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " *  " + GREEN_BRIGHT + "          THE FASTEST GUESSES          " + RESET + "* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " *  " + GREEN_BRIGHT + "            GET MORE POINTS            " + RESET + "* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " **                                       ** " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + " ******************************************* " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|" + RESET + "    " + WHITE_BOLD_BRIGHT + "      HOPE YOU HAVE FUN BITCH!!!         " + YELLOW_BOLD_BRIGHT + "|" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|   __________________________________________|___\n" +
            "    " + YELLOW_BOLD_BRIGHT + "|  /                                             /" + RED_BOLD_BRIGHT + ".\n" +
            "    " + YELLOW_BOLD_BRIGHT + "\\_/_____________________________________________/" + RED_BOLD_BRIGHT + "." + RESET + "\n" + "\n";


    public static final String START_GAME = "\n                 " + ASTERISKS + GREEN_BOLD_BRIGHT + " GO GO GO " + ASTERISKS + RESET + "\n";

    public static final String CORRECT = "\n                 " + ASTERISKS + GREEN_BOLD_BRIGHT + " NICE ONE " + ASTERISKS + RESET + "\n";

    public static final String FINISH_GAME = "\n           " + ASTERISKS + RED_BOLD_BRIGHT + " THE GAME IS NOW OVER " + ASTERISKS + "\n" +
            BLACK_BOLD_BRIGHT + "     (You can go back to your usual business... )\n" + " " +
            "         (... of dealing drugs or whatever)" + RESET + "\n";

    public static final String FINAL_SCORES = "\n                " + ASTERISKS + YELLOW_BOLD_BRIGHT + " HIGHSCORES " + ASTERISKS + RESET + "\n" +
            BLACK_BOLD_BRIGHT + "           (Press Enter to see all of them)" + RESET + "\n";

    public static String sendScore(long score) {
        return "\n                " + YELLOW_BOLD_BRIGHT + " YOU SCORED " + CYAN_BOLD_BRIGHT + score + " " + YELLOW_BOLD_BRIGHT + "POINTS" + RESET + "\n";
    }

    public static String sendTotalScore(String playerName, long score) {
        return "\n           " + PURPLE_BRIGHT + playerName.toUpperCase() + YELLOW_BOLD_BRIGHT + " TOTAL SCORE: " + CYAN_BOLD_BRIGHT + score + " " + YELLOW_BOLD_BRIGHT + "POINTS" + RESET + "\n";
    }

    public static String sendWinner(String playerName) {
        return "\n       " + ASTERISKS + RED_BOLD_BRIGHT + " C" + YELLOW_BOLD_BRIGHT + "O" + GREEN_BOLD_BRIGHT + "N" + CYAN_BOLD_BRIGHT + "G" + BLUE_BOLD_BRIGHT + "R" + PURPLE_BOLD_BRIGHT + "A" + BLUE_BOLD_BRIGHT + "T" + CYAN_BOLD_BRIGHT + "U" + GREEN_BOLD_BRIGHT + "L" + YELLOW_BOLD_BRIGHT + "A" + RED_BOLD_BRIGHT + "T" + YELLOW_BOLD_BRIGHT + "I" + GREEN_BOLD_BRIGHT + "O" + CYAN_BOLD_BRIGHT + "N" + BLUE_BOLD_BRIGHT + "S " +
                PURPLE_BRIGHT + playerName.toUpperCase() + BLUE_BOLD_BRIGHT + " Y" + CYAN_BOLD_BRIGHT + "O" + GREEN_BOLD_BRIGHT + "U" + YELLOW_BOLD_BRIGHT + " W" + RED_BOLD_BRIGHT + "O" + YELLOW_BOLD_BRIGHT + "N " + ASTERISKS + RESET + "\n" +
                "      " + BLACK_BOLD_BRIGHT + "    (Everyone else sucks and got rekt)" + RESET + "\n";
    }


    public static String writeWord(String word) {
        return "\n" + ASTERISKS + YELLOW_BOLD_BRIGHT + " WRITE THE WORD " + WHITE_BOLD_BRIGHT + "-> " + BLUE_BOLD_BRIGHT + word.toUpperCase() + WHITE_BOLD_BRIGHT + " <-" + YELLOW_BOLD_BRIGHT + " IF YOU HAVEN'T YET " + ASTERISKS + RESET + "\n";

    }

    public static String DisplayBar(String colour, int i) {

        String ZERO = "▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁▁";
        String ODD = "▄";
        String EVEN = "█";

        StringBuilder loadingBar = new StringBuilder();

        int x = i / 2;
        loadingBar.append(GameInfo.WHITE_BOLD_BRIGHT + "|");
        for (int k = 0; k < 50; k++) {
            loadingBar.append(String.format(colour + "%s", ((x <= k) ? " " : EVEN)));
        }
        loadingBar.append(GameInfo.WHITE_BOLD_BRIGHT + "|");

        return loadingBar.toString();

    }






    public static final String CONNECT_TO_IMAGE_SERVER = "\n        " + ASTERISKS + RED_BOLD_BRIGHT + " CONNECT TO THE IMAGE SERVER " + ASTERISKS + "\n" +
            "          " + ASTERISKS + YELLOW_BOLD_BRIGHT + " IP" + YELLOW_BOLD_BRIGHT + ":" + GREEN_BOLD_BRIGHT + " SAME ONE " + YELLOW_BOLD_BRIGHT + "PORT" + YELLOW_BOLD_BRIGHT + ":"+ GREEN_BOLD_BRIGHT + " 5050 " + ASTERISKS + "\n" +
            "       " + BLACK_BOLD_BRIGHT + "(Else you won't be able to play the game)" + "\n";


    public static final String CHAT_DENIED = "\n               " + ASTERISKS + RED_BOLD_BRIGHT + " CHAT DISABLED " + ASTERISKS + "\n" +
            "           " + BLACK_BOLD_BRIGHT + "(Everyone has to guess correctly)" + "\n" +
            "      " + BLACK_BOLD_BRIGHT + "(Rejoice while it lasts and Praise the Sun)" + RESET + "\n";


    public static final String CHECK_READY = "\n" + "        " + ASTERISKS + YELLOW_BOLD_BRIGHT + " WHEN YOU'RE READY TYPE: " + GREEN_BOLD_BRIGHT + "YES " + ASTERISKS + RESET + "\n";


    public static final String WAIT = "\n" + "   " + ASTERISKS + YELLOW_BOLD_BRIGHT + " WAITING FOR ALL PLAYERS TO CONNECT... " + ASTERISKS + "\n" +
            "         " + BLACK_BOLD_BRIGHT + "(... cool elevator music playing...)" + RESET + "\n";






}
